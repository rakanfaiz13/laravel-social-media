<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Utama</title>
</head>

<body>
    <h1>SanberBook</h1>

    <section class="intro">
        <h2>Social Media Developer Santai Berkualitas</h2>
        <p>Belajar dan berbagi hidup ini semakin santai berkualitas</p>
    </section>

    <section class="benefit">
        <h3>Benefit Join di SanberBook</h3>
        <div class="list-benefit">
            <ul type="disc">
                <li>Mendapatkan motivasi dari sesama developer</li>
                <li>Sharing knowledge dari para mastah sanber</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>
        </div>
    </section>

    <section class="join">
        <h3>Cara Bergabung ke SanberBook</h3>
        <div class="list-join">
            <ol>
                <li>Mengunjungi Website ini</li>
                <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
                <li>Selesai!</li>
            </ol>
        </div>
    </section>
</body>

</html>