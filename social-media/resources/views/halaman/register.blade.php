<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Register</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <div class="fn">
            <p> First Name: </p>
            <input type="text" name="firstname">
        </div>
        <div class="ln">
            <p> Last Name: </p>
            <input type="text" name="lastname">
        </div>

        <p>Gender:</p>
        <input type="radio" id="male" name="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="other">
        <label for="other">Other</label>

        <br>

        <p>Nationality:</p>
        <select name="nationalities" id="nationalities">
            <option value="indonesia">Indonesian</option>
            <option value="singapore">Singaporean</option>
            <option value="malaysia">Malaysian</option>
            <option value="australia">Australian</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" id="idn" name="idn-lang">
        <label for="idn">Bahasa Indonesia</label><br>
        <input type="checkbox" id="eng" name="eng-lang">
        <label for="eng">English</label><br>
        <input type="checkbox" id="other-lang" name="other-lang">
        <label for="other-lang">Other</label>

        <p>Bio:</p>
        <textarea name="biodata" id="bio" cols="30" rows="10"></textarea>

        <br>

        <button type="submit">Sign Up</button>
    </form>
</body>

</html>